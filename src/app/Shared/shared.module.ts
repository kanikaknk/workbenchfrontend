import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { CommonModule } from '@angular/common';
import {
  NbThemeModule,
  NbSidebarModule,
  NbLayoutModule,
  NbButtonModule,
  NbSidebarService
} from '@nebular/theme';

@NgModule({
  declarations: [
    HeaderComponent,
    SidebarComponent
  ],
  imports: [
    // BrowserModule,
    // AppRoutingModule, 
    // NbActionsModule, NbIconModule, NbSearchModule,
    CommonModule,
    NbThemeModule,
    NbSidebarModule,
    NbLayoutModule,
    NbButtonModule,
  ],
  exports: [
        SidebarComponent,
        HeaderComponent
  ],

  providers: [NbSidebarService],

})
export class SharedModule { 

}
